# Takes a CSV file of device variables and turns it into YAML for use
# by Ansible/Jinja2 config generation process
#
#
# This software is provided with no warranty

#!/usr/bin/python

__author__ = "Andrew Mulheirn"
__version__ = "0.3"
__maintainer__ = "Andrew Mulheirn"
__email__ = "a.mulheirn@infradata.com"
__status__ = "Test"


import csv

# Open the file and read the contents in as dictionary

with open('source-variables.csv', encoding='utf-8-sig') as f:
    reader = csv.DictReader(f, dialect='excel')

    # Write the YAML version
    varfile = open('./roles/confgen2/vars/main.yml', 'w', encoding='utf-8')
    print("---")
    varfile.write("---\n")
    print("config_parameters:")
    varfile.write("config_parameters:\n")
    try:
        for row in reader:
            varfile.write("  - { ",)
            print("  - { ",)
            for item, value in row.items():
                varfile.write("%s: %s, " % (item, value))
                print(item, ": ", value, ",",)
            varfile.write("}\n")
            print("}")
    except csv.Error as e:
        sys.exit('file %s, line %d: %s' % (f, reader.line_num, e))
